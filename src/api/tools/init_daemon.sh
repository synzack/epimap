#!/bin/bash

# Deamon start/stop script for debian

. /lib/lsb/init-functions

cd `dirname $0`
PIDFILE=./.daemon.lock
EXECFILE=`pwd`/update_daemon.sh
NAME=epimap
DESC="Epimap update script"

case $1 in
    start)
        log_daemon_msg "Starting $DESC" "$NAME"
        if [ -f $PIDFILE ]; then
            kill `cat $PIDFILE` && rm $PIDFILE
            if [ $? != 0 ]; then
                log_failure_msg "pid file '$PIDFILE' exists."
                exit 1
            fi
        fi
        /sbin/start-stop-daemon --start --quiet --pidfile "$PIDFILE" \
            --exec "$EXECFILE" -m -b -d `pwd`
        log_end_msg $?
        ;;
    stop)
        if [ -z "$PIDFILE" ]; then
            log_failure_msg "file '$PIDFILE' does not exist."
            exit 1
        fi
        log_daemon_msg "Stopping $DESC" "$NAME"
        /sbin/start-stop-daemon --stop --quiet --retry 5\
            --pidfile "$PIDFILE" #--exec $EXECFILE -d `pwd`
        log_end_msg $?
        rm -f "$PIDFILE"
        ;;
    restart)
        ./$0 stop
        ./$0 start
        ;;
    *)
        echo "Usage: $0 {start|stop|restart}"
        exit 1
esac

