#!/bin/bash

DELAY=5m
cd `dirname $0`/..
LOGFILE=./update.log

while true; do
    python ./server.py update &> $LOGFILE
    sleep $DELAY
done
