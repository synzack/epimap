#!/usr/bin/python

import os
from database import Room, User, Computer
from xml.dom import minidom
from xml.parsers.expat import ExpatError

class SVGHandler(object):
    def _load_file(self, file_name):
        """ Loads a svg and turn it into a Room """
        def parse_error(text):
            self.logger.error("%s: %s"
                    % (svg_file, text))

        try:
            svg = minidom.parse(file_name)
        except ExpatError, err:
            parse_error(err)
            return
        try:
            name = svg.getElementsByTagName("title")[0].childNodes[0].data
            room = Room(name, svg)
        except IndexError, AttributeError:
            parse_error("`title' tag missing or malformed")
            return

        computers = svg.getElementsByTagName("use")
        if not computers:
            parse_error("no `use' tag")
        else:
            room.capacity = len(computers)
            for comp in computers:
                try:
                    ip = comp.attributes["id"].value
                    if ip.endswith("?"):
                        continue
                    x = float(comp.attributes["x"].value)
                    y = float(comp.attributes["y"].value)
                except (KeyError, ValueError), err:
                    parse_error("a computer has a wrong attribute: %s" % err)
                    return
                c = Computer(room, ip, x, y)
                self.db.session.add(c)
            self.db.session.add(room)
            self.db.session.commit()

    def load_all(self, db_handler, svgdir="svg/"):
        """ fill the databse with the svg files """
        self.db = db_handler.db
        # use the app logger (this is pretty ugly)
        self.logger = db_handler._app.logger
        files = [f for f in os.listdir(svgdir) if f.endswith(".svg")]
        for file in files:
            self._load_file(os.path.join(svgdir, file))

    def gen(self, room=None, user=None):
        """ Returns a svg filled with users """
        assert isinstance(room, Room) or isinstance(user, User)
        # fetch the svg content in the database
        if user:
            svg = user.room.svg
        else:
            svg = room.svg
        svg_node = svg.firstChild
        new_svg = svg_node.cloneNode(False)
        new_svg.removeAttribute("viewBox")
        rwidth = int(svg_node.getAttribute("width").replace("px", ""))
        rheight = int(svg_node.getAttribute("height").replace("px", ""))
        new_svg.setAttribute("width", "%dpx" % rwidth)
        new_svg.setAttribute("height", "%dpx" % rheight)
        new_svg.appendChild(svg_node.cloneNode(True))
        width, height = [int(i) for i in
                svg_node.getAttribute("viewBox").split()[2:]]
        # we need the absolute size in pixels to ignore the user defined scale
        svg.replaceChild(new_svg, svg_node)

        ratio = int(rheight) / int(height), int(rwidth) / int(width)

        for computer in svg.getElementsByTagName("use"):
            ip = computer.getAttribute("id")
            comp = Computer.query.filter_by(ip=ip).first()
            if ip.endswith("?"):
                continue
            if not comp.user or (user and comp.user != user):
                computer.setAttribute("class", "avail")
            else:
                text = svg.createElement("text")
                o = computer.getAttribute("xlink:href")
                text_content = svg.createTextNode(comp.user.name)
                text.appendChild(text_content)
                text.setAttribute("x", str(comp.x * ratio[1]
                    + (-35 if o == "#pc3" else (35 if o == "#pc4" else 8))))
                text.setAttribute("y", str(rheight - comp.y * ratio[0]
                    - (28 if o == "#pc2" else (-36 if o == "#pc1" else -20))))
                text.setAttribute("fill", "#447")
                text.setAttribute("font-size", ".8em")
                text.setAttribute("text-anchor", "middle")
                text.setAttribute("stroke", "white")
                text.setAttribute("stroke-width", "0.2px")
                text.setAttribute("font-weight", "bold")
                new_svg.appendChild(text)
        return svg

