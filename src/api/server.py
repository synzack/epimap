#!/usr/bin/python

from flask import Flask, Response, request, abort
from sys import argv
from werkzeug.contrib.cache import FileSystemCache
import logging
from logging.handlers import SMTPHandler

from database import db, DBHandler, User, Room
from svg import SVGHandler
from xml_wrapper import XMLWrapper
import errors

app = Flask(__name__)
app.config.from_pyfile("server.cfg")
svg_handler = SVGHandler()
db.init_app(app)
wrapper = XMLWrapper(app.config["DEBUG"])
cache = FileSystemCache("cache/", default_timeout=60)
#mail_handler = SMTPHandler('smtp.free.fr ', "server-error@demic.eu",
#        app.config["ADMINS"], "Epimap API failed")
#app.logger.addHandler(mail_handler)

@app.route("/")
def index():
    address = "%s/room.svg?room=%%s" % request.url_root
    code = []
    for room in ["sr", "midlab", "cisco", "sm14"]:
        code.append("<embed src=\"%s\" type=\"image/svg+xml\" />"
                % (address % room))
    return "".join(code)

@app.route("/user.xml")
def user():
    if "login" in request.args:
        name = request.args["login"]
        user = User.query.filter_by(name=name).first()
        if user:
            return wrapper.user_infos([user])
        else:
            return wrapper.error(errors.USER_NOT_CONNECTED, name)
    elif "ip" in request.args:
        ip = request.args["ip"]
        user = User.query.filter_by(ip=ip).first()
        if user:
            return wrapper.user_infos([user])
        else:
            return wrapper.error(errors.IP_NOT_USED, ip)
    users = User.query.filter(User.room_name != "None")
    return wrapper.user_infos(users)

@app.route("/room.xml")
def room():
    if "name" in request.args:
        name = request.args["name"]
        room = Room.query.filter_by(name=name).first()
        if room:
            rooms = [room]
        else:
            return wrapper.error(errors.UNKNOWN_ROOM, name)
    else:
        rooms = Room.query.all()
    return wrapper.room_infos(rooms, "summary" in request.args) 

@app.route("/room.svg")
def room_svg():
    if "room" in request.args:
        name = request.args["room"]
        room = Room.query.filter_by(name=name).first()
        if room:
            return Response(svg_handler.get(room=room).toxml(),
                    mimetype="image/svg+xml")
        else:
            abort(400)
    elif "user" in request.args:
        name = request.args["user"]
        user = User.query.filter_by(name=name).first()
        if user:
            return Response(svg_handler.get(user=user).toxml(),
                    mimetype="image/svg+xml")
        else:
            abort(400)
    else:
        abort(400)

@app.before_request
def return_cached():
    if app.config["DEBUG"]:
        return
    response = cache.get(request.url)
    if response:
        return response

@app.after_request
def cache_reponse(response):
    if app.config["DEBUG"]:
        return response
    if response.status_code == 200:
        cache.set(request.url, response)
    return response


def with_context(fun):
    def new_fun(*args, **kwargs):
        ctx = app.test_request_context()
        ctx.push()
        ret = fun(*args, **kwargs)
        ctx.pop()
        return ret
    return new_fun

@with_context
def create_all():
    db.create_all()

@with_context
def drop_all():
    db.drop_all()

if __name__ == "__main__":
    for arg in argv[1:]:
        if arg == "init":
            create_all()
        elif arg == "clear":
            drop_all()
        elif arg == "fill":
            drop_all()
            create_all()
            u = DBHandler(app)
            svg_handler.load_all(u)
            u.fill_users()
        elif arg == "update":
            u = DBHandler(app)
            u.update()
        elif arg == "run":
            app.run(host="0.0.0.0", debug=True)

