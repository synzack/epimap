
(
    NO_ERROR,
    USER_NOT_CONNECTED,
    IP_NOT_USED,
    UNKNOWN_ROOM,
) = xrange(4)

errors = {
        NO_ERROR:           "No error",
        USER_NOT_CONNECTED: "User %s is not connected",
        IP_NOT_USED:        "IP %s is not used",
        UNKNOWN_ROOM:       "Room %s does not exist",
}
