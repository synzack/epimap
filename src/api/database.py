from flask.ext.sqlalchemy import SQLAlchemy
from netsoul.netsoul import NetsoulExternal, NetsoulError
import socket
from xml.dom import minidom

db = SQLAlchemy()

class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(7))
    status = db.Column(db.String(6))
    computer = db.relationship("Computer", uselist=False, backref="user")
    hostname = db.Column(db.String(50))
    room_name = db.Column(db.String(20), db.ForeignKey("room.name"))
    constraint = db.UniqueConstraint('computer')

    def __init__(self, name, status, computer, hostname):
        self.name = name
        self.status = status
        self.hostname = hostname
        self.computer = computer
        room = Room.query.get(computer.room_name)
        assert room is not None
        room.add_user(self)

    def __repr__(self):
        return "<User '%s', room '%s', ip '%s'>" % (self.name, self.room_name,
                self.computer.ip if self.computer else "???")


class Room(db.Model):
    __tablename__ = "room"
    name = db.Column(db.String(20), primary_key=True)
    available = db.Column(db.Boolean)
    capacity = db.Column(db.Integer)
    svg = db.Column(db.PickleType)
    users = db.relationship("User", backref="room", lazy="dynamic")
    computers = db.relationship("Computer", backref="room", lazy="dynamic")

    def __init__(self, name, svg=None, capacity=0):
        self.name = name
        self.available = True
        self.capacity = capacity
        if svg:
            assert isinstance(svg, minidom.Document)
        self.svg = svg

    def __repr__(self):
        return "<Room '%s'>" % self.name

    def add_user(self, user):
        assert isinstance(user, User)
        user.room = self
        user.room_name = self.name

    def add_computer(self, computer):
        assert isinstance(computer, Computer)
        computer.room = self
        computer.room_name = self.name


class Computer(db.Model):
    __tablename__ = "computer"
    room_name = db.Column(db.String(20), db.ForeignKey("room.name"))
    ip = db.Column(db.String(39), primary_key=True)
    x = db.Column(db.SmallInteger)
    y = db.Column(db.SmallInteger)
    # 0 to 100
    life = db.Column(db.Integer)
    user_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    constraint = db.UniqueConstraint('user')

    def __init__(self, room, ip, x, y):
        self.ip = ip
        self.x = x
        self.y = y
        self.user_id = None
        self.life = 100
        self.room = room

    def __repr__(self):
        return "<Computer '%s'>" % self.ip


class DBHandler(object):
    """ Use a netsoul connection to fill and update the databse """
    ns = None
    def __init__(self, app):
        self.db = db
        self._app = app
        self._connection_trys = 0
        self._fun_trys = 0
        self._init()

    def __del__(self):
        if self.ns is not None:
            self.ns.close()
        if self.ctx:
            self.ctx.pop()

    def _clean_hostname(self, host):
        if host.startswith("pc-"):
            return host[3:]
        return host

    def shitty_netsoul_connection(fun):
        """
        wrap a function in order to limit the connection fails
        """
        def new_fun(self, *args, **kwargs):
            try:
                if not self.ns and fun.__name__ != "_init":
                    raise NetsoulError("Not connected to netsoul")
                ret = fun(self, *args, **kwargs)
                self._fun_trys = 0
                return ret
            except NetsoulError, err:
                self._fun_trys += 1
                if self._fun_trys == 3:
                    self._app.logger.error("Couldn't call %s: %s" 
                            % (fun.__name__, str(err)))
                    self._fun_trys = 0
                else:
                    self._init()
                    return new_fun(self)
                return None

        return new_fun

    @shitty_netsoul_connection
    def _init(self):
        try:
            self.ns = NetsoulExternal()
            self.ns.handshake()
            self._connection_trys = 0
        except NetsoulError, err:
            self._connection_trys += 1
            if self._connection_trys == 3:
                self._app.logger.error("Netsoul error: %s" % str(err))
                self.ns = None
                self._connection_trys = 0

        if hasattr(self._app, "test_request_context"):
            self.ctx = self._app.test_request_context()
            self.ctx.push()
        else:
            # The flask test client doesn't have (and doesn't need) a
            # test_request_context
            self.ctx = None

    @shitty_netsoul_connection
    def update(self):
        """
        Checks the difference between the db and the new netsoul query,
        and update the db.
        """
        self._app.logger.debug("Starting update...")
        users = self.ns.list_users()
        hosts = self.ns.list_hosts()
        old_users = User.query.filter(User.room_name != "None").all()
        new_set = set([(unicode(u.user), unicode(u.ip)) for u
            in users])
        host_set = set([(unicode(self._clean_hostname(h.host)), unicode(h.ip))
            for h in hosts])
        new_set.update(host_set)
        old_set = set([(u.name, u.computer.ip) for u in old_users if u.computer])
        delete_users = old_set - new_set
        add_users = new_set - old_set
        #self._app.logger.debug("Add: %s\nDelete: %s" % (add_users,
        #    delete_users))
        self.delete_users(delete_users)
        self.add_users(u for u in users if (u.user, u.ip) in add_users)

    def fill_rooms(self):
        for room in self.ns.rooms:
            db.session.add(Room(room, capacity=self.ns.rooms[room].capacity))
        db.session.commit()

    def add_hosts(self, hosts):
        """
        Checks if an host is not aready in the db and add it
        """
        for host in hosts:
            computer = Computer.query.filter_by(ip=host.ip).first()
            if computer and not computer.user:
                u = User(self._clean_hostname(host.host), "-", computer,
                        host.location)
                db.session.add(u)
        db.session.commit()


    def add_users(self, users):
        """
        Adds the users (a list of netsoul.User) to the database
        """
        for user in users:
            computer = Computer.query.filter_by(ip=user.ip).first()
            if computer:
                u = User(user.user, user.status, computer,
                    user.location)
                db.session.add(u)
        db.session.commit()

    def delete_users(self, users):
        """
        Remove the users (a list of (login, room)) from the database
        """
        for u in users:
            du = User.query.filter_by(name=u[0]).join(User.computer).filter_by(
                    ip=u[1]).first()
            User.query.filter_by(id=du.id).delete()
        db.session.commit()

    @shitty_netsoul_connection
    def fill_users(self):
        self.add_users(self.ns.list_users())
        self.add_hosts(self.ns.list_hosts())
