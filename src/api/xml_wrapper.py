from xml.dom.minidom import Document, Node
from errors import errors, USER_NOT_CONNECTED


def string_output(fun):
    def new_fun(self, *args, **kwargs):
        ret = fun(self, *args, **kwargs)
        assert isinstance(ret, Node)
        return ret.toprettyxml() if self.debug else ret.toxml()
    return new_fun


class XMLWrapper(object):
    def __init__(self, debug=False):
        """ if debug is True, returns pretty xml with indentation """
        self.debug = debug
        self.base = Document()

    def _base(self, status):
        """
        create a `ns' root node with a status attribute
        return a ns Node
        """
        self.base = Document()
        ns = self.base.createElement("ns")
        ns.setAttribute("status", status)
        self.base.appendChild(ns)
        return ns

    def _text_node(self, name, value, **kwargs):
        """
        Create a text node <name attr=val...>value</name>
        """
        node = self.base.createElement(name)
        text_node = self.base.createTextNode(value)
        node.appendChild(text_node)
        for k, v in kwargs.iteritems():
            node.setAttribute(k, v)
        return node

    def _user(self, user):
        """ Create a node from a database.User object """
        user_node = self.base.createElement("user")
        user_node.appendChild(self._text_node("login", user.name))
        user_node.appendChild(self._text_node("status", user.status))
        ip = user.computer.ip if user.computer else "???"
        user_node.appendChild(self._text_node("ip", ip, version="4"))
        user_node.appendChild(self._text_node("hostname", user.hostname))
        return user_node

    def _room(self, room, summary=False):
        """ Create a node from database.Room object """
        room_node = self.base.createElement("room")
        room_node.setAttribute("name", room.name)
        room_node.setAttribute("available", "yes" if room.available else "no")
        if room.capacity:
            room_node.setAttribute("capacity", str(room.capacity))
        if room.users:
            room_node.setAttribute("users", str(room.users.count()))
        if not summary:
            for user in room.users:
                room_node.appendChild(self._user(user))
        # TODO: append reservations
        return room_node

    def _one_user_infos(self, user):
        user_node = self._user(user)
        if user.room_name != "None":
            loc_node = self.base.createElement("location")
            loc_node.setAttribute("x", str(user.computer.x))
            loc_node.setAttribute("y", str(user.computer.y))
            room_node = self.base.createElement("room")
            room_node.setAttribute("name", user.room_name)
            loc_node.appendChild(room_node)
            user_node.appendChild(loc_node)
        return user_node

    @string_output
    def error(self, code=0, *args):
        ns = self._base("error")
        error_node = self._text_node("error", errors[code] % args,
                code=str(code))
        ns.appendChild(error_node)
        return self.base

    @string_output
    def room_infos(self, rooms, summary=False):
        """
        rooms is a list of database.Room to describe
        """
        ns = self._base("ok")
        for room in rooms:
            room_node = self._room(room, summary)
            ns.appendChild(room_node)
        return self.base

    @string_output
    def user_infos(self, users):
        """
        user is a database.User instance
        """
        ns = self._base("ok")
        for user in users:
            ns.appendChild(self._one_user_infos(user))
        return self.base


if __name__ == "__main__":
    wrapper = XMLWrapper(True)
    print wrapper.error(USER_NOT_CONNECTED, "foo_x")
    from database import User, Room

    u1 = User("foo_x", "away", "120.12.34.21", "blop")
    u2 = User("bar_x", "active", "120.12.34.22", "bite")
    room = Room("foo")
    room.add_user(u1)
    room.add_user(u2)
    print wrapper.user_infos([u1, u2])
    print wrapper.room_infos([room])
