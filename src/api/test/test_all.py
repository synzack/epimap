import test_flask
import test_netsoul
import inspect
import unittest

if __name__ == "__main__":
    modules = [test_flask, test_netsoul]
    suite = unittest.TestSuite()
    for module in modules:
        suite.addTest(module.suite)
    unittest.TextTestRunner(verbosity=2).run(suite)

    

