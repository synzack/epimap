import unittest
import os
import tempfile
from sys import stdout, path
path.append(".")
import database
import server
import re

db = None

class FlaskTestCase(unittest.TestCase):
    db_fd = None

    def __del__(self):
        if self.db_fd:
            os.close(self.db_fd)
            self.__class__.db_fd = None
            os.unlink(server.app.config['DATABASE'])

    def setUp(self):
        if self.db_fd is None:
            (self.__class__.db_fd,
                    server.app.config['DATABASE']) = tempfile.mkstemp()
        server.app.config['TESTING'] = True
        self.client = server.app.test_client()
        
    def test_1_fill_db(self):
        server.drop_all()
        server.create_all()
        db = database.DBHandler(server.app)
        server.svg_handler.load_all(db)
        db.fill_users()

    def test_2_home(self):
        assert self.client.get("/").status_code == 200

    def _test_xml_structure(self, content, error=False):
        status = "error" if error else "ok"
        pattern = re.compile(r"^[ \n]*<\?xml +version=\"[0-9\.]+\" *\?>",
                re.M | re.I)
        assert pattern.match(content) is not None
        pattern = re.compile(r"<ns status=\"%s\">.*</ns>[\n ]*$" % status,
                re.M | re.I | re.S)
        assert pattern.search(content) is not None

    def _test_xml_contains_all_rooms(self, content):
        # FIXME: complete this
        rooms = ("cisco", "sr", "midlab", "sm14")
        for room in rooms:
            pattern = re.compile(r"<room[^>]*name=\"%s\"[^>]*>" % room, re.M)
            self.assertNotEqual(pattern.search(content), None,
                    msg="room %s is missing" % room)

    def _test_well_formed_xml(self, content):
        # FIXME
        pass

    def test_3_room_xml(self):
        ret = self.client.get("/room.xml")
        assert ret.status_code == 200
        self._test_xml_structure(ret.data)
        self._test_xml_contains_all_rooms(ret.data)

    def test_4_room_xml(self):
        ret = self.client.get("/room.xml?name=cisco")
        assert ret.status_code == 200
        self._test_xml_structure(ret.data)
        assert ret.data.count("<room") == 1

    def test_5_room_xml(self):
        ret = self.client.get("/room.xml?name=foo")
        assert ret.status_code == 200
        self._test_xml_structure(ret.data, error=True)
        #TODO: check error type

    def test_6_room_xml(self):
        ret = self.client.get("/room.xml?foo=pasteur")
        assert ret.status_code == 200
        self._test_xml_structure(ret.data)
        self._test_xml_contains_all_rooms(ret.data)

    def tearDown(self):
        pass

suite = unittest.TestLoader().loadTestsFromTestCase(FlaskTestCase)

if __name__ == "__main__":
    unittest.main()
