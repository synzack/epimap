import unittest
import sys
from sys import stdout, path
path.append(".")
from netsoul import netsoul
from netsoul.room import Room
from netsoul.user import User

def multiple_tests(count):
    def wrap(fun):
        def new_fun(self):
            for i in xrange(count):
                stdout.write(".")
                stdout.flush()
                fun(self)
        return new_fun
    return wrap

class NetsoulTestCase(unittest.TestCase):
    def setUp(self):
        self.ns = netsoul.NetsoulExternal()
        self.ns.handshake()

    def tearDown(self):
        self.ns.close()

    def test_1_rooms(self):
        self.assertNotEqual(self.ns.rooms, {})
        for room_name in ("cisco", "midlab", "sr", "pasteur1", "pasteur2",
                "sm14", "sm15", "sm02", "sm03"):
            self.assertTrue(room_name in self.ns.rooms)
            self.assertTrue(isinstance(self.ns.rooms[room_name], Room))

    @multiple_tests(5)
    def test_2_list(self):
        users = self.ns.list_users()
        self.assertTrue(isinstance(users, list))
        self.assertTrue(users is not None)
        self.assertNotEqual(len(users), 0)
        for u in users:
            self.assertTrue(isinstance(u, User))

    def test_3_filled_rooms(self):
        self.ns.list_users()
        rooms = self.ns.rooms.values()
        user_set = set.intersection(*[set(r.users) for r in rooms])
        # test if all users are unique
        self.assertTrue(len(user_set) == 0,
                msg="uniqueness of the users")
        user_set = set.union(*[set(r.users) for r in rooms])
        # test if the rooms have been filled (not very reliable)
        self.assertFalse(len(user_set) == 0, msg="""
        Warning: This test assumes there is at least someone conected in a room.
        """)

suite = unittest.TestLoader().loadTestsFromTestCase(NetsoulTestCase)

if __name__ == "__main__":
    unittest.main()
