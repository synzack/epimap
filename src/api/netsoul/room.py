class Room(object):
    def __init__(self, name, pattern, capacity=0):
       self.name = name
       self.pattern = pattern
       self.users = {}
       self.hosts = {}
       self.capacity = capacity

    def __repr__(self):
        return self.name

    def __str__(self):
        return self.name

    def match(self, user):
        """
        Sets the users' room if it matches, and append it to the users list
        """
        if user.ip.startswith(self.pattern):
            user.room = self
            self.users[user.user] = user
            return True
        return False

    def match_host(self, host):
        """
        Sets the hosts' room if it matches, and append it to the host list
        """
        if host.ip.startswith(self.pattern):
            host.room = self
            self.hosts[host.host] = host
            return True
        return False
