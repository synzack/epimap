from room import Room

class User(object):
    def __init__(self, line):
        """ Parse the line, sets the users' attributes """
        fields = line.split()

        for i, k in enumerate( ("socket", "user", "ip", "login-ts",
            "last-change-ts", "trust-level-low", "trust-level-high", "type",
            "location", "group", "status", "udata") ):
            setattr(self, k, fields[i])
        self.room = None

    def __getitem__(self, elem):
        return getattr(self, elem)

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<User '%s', room '%s'>" % (self.user, self.room)
    
    def set_room(self, rooms):
        """
        Take a list of Room as argument, and sets the matching room.
        return True if a room matched.
        """
        # fails if rooms is not itrable
        rooms_iter = iter(rooms)
        for room in rooms:
            assert isinstance(room, Room)
            if room.match(self):
                return True
        return False

