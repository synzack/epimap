#!/usr/bin/python2

import socket
from threading import Thread, Event
from urllib import quote, unquote
from time import sleep
import logging
from pprint import pprint

from room import Room
from user import User
from host import Host

REMOTE_SERVER = "ns-server.epitech.eu"
REMOTE_PORT = 4242

LOCAL_SERVER = "localhost"
LOCAL_PORT = 4747

logging.basicConfig(level=logging.WARNING)


class Message(object):
    msgs = []
    def __init__(self):
        Event.__init__(self)

    def set(self, msg):
        self.msgs.append(msg)

    def get(self):
        try:
            return self.msgs.pop(0)
        except IndexError:
            return None

    def is_set(self):
        return self.msgs != []


class Sender(Thread):
    def __init__(self, netsoul, halt):
        self.ns = netsoul
        self.halt = halt
        Thread.__init__(self)

    def run(self):
        ns = self.ns
        m = ns.message
        while not self.halt.is_set():
            if m.is_set():
                msg = ns._send(m.get())
            sleep(0.1)


class Receiver(Thread):
    def __init__(self, netsoul, halt):
        self.ns = netsoul
        self.halt = halt
        Thread.__init__(self)

    def run(self):
        ns = self.ns
        while not self.halt.is_set():
            ns.recv()
        sleep(0.5)



class NetsoulError(Exception):
    pass


class NetsoulBase(object):
    """
    This class handles the netsoul connection and the messages.
    """
    stop = Event()
    # commands contains the commands to send to the server
    # It can be overwritten in NetsoulExternal
    commands = {
            "send_msg": "user_cmd msg *:%(user)s@* msg %(msg)s",
            "who": "user_cmd who %(user)s",
            "list_users": "list_users",
            "list_hosts": "list_hosts",
            }
    hooks = {}
    msg_fmt = "\033[92m%s > %s\033[0m"
    # XXX delete this
    rooms = {
        "cisco":    Room("cisco", "10.250"),
        "midlab":   Room("midlab", "10.249"),
        "sr":       Room("sr", "10.248", capacity=75),
        "pasteur1": Room("pasteur1", "10.247"),
        # FIXME: change ip
        "pasteur2": Room("pasteur2", "10.247"),
        "sm14":     Room("sm14", "10.200.14"),
        "sm15":     Room("sm15", "10.200.15"),
        "sm02":     Room("sm02", "10.200.02"),
        "sm03":     Room("sm03", "10.200.03"),
    }

    def socket_wrap(fun):
        def new_fun(*args, **kwargs):
            try:
                return fun(*args, **kwargs)
            except (socket.error, socket.timeout), err:
                raise NetsoulError(err)

        return new_fun

    @socket_wrap
    def __init__(self, host, port, recv_thread = True, send_thread = True):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM,
                socket.IPPROTO_TCP)
        self.s.settimeout(5)
        self.s.connect((host, port))
        self.message = Message()
        if recv_thread:
            self.t_recv = Receiver(self, self.stop)
            self.t_recv.daemon = True
            self.t_recv.start()
        else:
            self.t_recv = None
        if send_thread:
            self.t_send = Sender(self, self.stop)
            self.t_send.daemon = True
            self.t_send.start()
        else:
            self.t_send = None

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    @socket_wrap
    def _send(self, cmd):
        """Send a command through the socket."""
        s_cmd = cmd
        if not isinstance(cmd, str):
            s_cmd = ' '.join(cmd);
        logging.debug("Sending `%s'" % s_cmd)
        self.s.send(s_cmd + '\n')

    def send(self, cmd):
        # Adds the message to the stack
        if self.t_send and cmd:
            self.message.set(cmd)
        elif cmd:
            self._send(cmd)

    @socket_wrap
    def recv(self):
        """ Fetch raw messages """
        msg = self.s.recv(4096)

        logging.debug("Received `%s'", msg)
        l_msg = msg.split()
        if not msg:
            return None

        return msg

    def send_msg(self, user, msg):
        args = {
                "msg": quote(msg, safe=''),
                "user": user
               }
        cmd = self.commands["send_msg"] % args
        self.send(cmd)

    def handshake(self):
        msg = self.recv() # hello msg
        assert msg.startswith("salut")
        assert len(msg.split("\n")) == 2
        return msg

    def list_users(self):
        """
        Send a list_user command, and waits for an answer.

        return a list of User objects
        throws NetsoulError if the user fetching fails.
        """
        self.send(self.commands["list_users"])
        content = []
        msg = ""
        n = 1000
        while not msg.endswith("cmd end\n") and n:
            msg = self.recv()
            if not msg:
                raise NetsoulError("Couldn't fetch user list")
            content.append(msg)
            n -= 1
        assert content[-1].endswith("cmd end\n")

        users = [User(line) for line in "".join(content).split("\n")[:-2]]
        rooms = self.rooms
        for u in users:
            u.set_room(rooms.itervalues())
        return users

    def list_hosts(self):
        """
        Send a list_hosts command
        """
        self.send(self.commands["list_hosts"])
        content = []
        msg = ""
        n = 1000
        while not msg.endswith("cmd end\n") and n:
            msg = self.recv()
            if not msg:
                raise NetsoulError("Couldn't fetch hosts list")
            content.append(msg)
            n -= 1
        assert content[-1].endswith("cmd end\n")

        hosts = [Host(line) for line in "".join(content).split("\n")[:-2]]
        rooms = self.rooms
        for h in hosts:
            h.set_room(rooms.itervalues())
        return hosts

    @socket_wrap
    def close(self):
        self.stop.set()
        self.s.close()


class NetsoulInternal(NetsoulBase):
    """ Use an existing connection """
    def __init__(self, recv_thread=True, send_thread=True):
        NetsoulBase.__init__(self, LOCAL_SERVER, LOCAL_PORT,
                send_thread=send_thread, recv_thread=recv_thread)


class NetsoulExternal(NetsoulBase):
    def __init__(self, recv_thread=False, send_thread=False):
        NetsoulBase.__init__(self, REMOTE_SERVER, REMOTE_PORT,
                send_thread=send_thread, recv_thread=recv_thread)


if __name__ == "__main__":
    with NetsoulExternal() as ns:
        ns.handshake()
        ns.list_users()
        ns.list_hosts()
