from room import Room

class Host(object):
    def __init__(self, line):
        """ Parse the line, sets the hosts' attributes """
        fields = line.split()

        #145 pc-brucke_j 10.250.3.4 1335739582 Fedora - - 1 5
        for i, k in enumerate( ("socket", "host", "ip", "login-ts",
            "type", "location", "com", "auth_user", "auth_ag") ):
            setattr(self, k, fields[i])
        self.room = None

    def __getitem__(self, elem):
        return getattr(self, elem)

    def __str__(self):
        return self.__repr__()

    def __repr__(self):
        return "<Host '%s', room '%s'>" % (self.host, self.room)
    
    def set_room(self, rooms):
        """
        Take a list of Room as argument, and sets the matching room.
        return True if a room matched.
        """
        # fails if rooms is not iterable
        rooms_iter = iter(rooms)
        for room in rooms:
            assert isinstance(room, Room)
            if room.match_host(self):
                return True
        return False

