\documentclass[a4paper,11pt,draft]{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[english]{isodate}
\usepackage[parfill]{parskip}
\usepackage[final]{listings}
\usepackage{fixme}
\usepackage[final,colorlinks]{hyperref}

\lstset{language=XML}


\title{NetSoul API reference}
\author{Jean-Philippe Brucker}

\begin{document}
    \maketitle
    \begin{abstract}
        This document explains the behavior of the NetSoul API, which is part of
        the project \textit{Epimap}. It mainly describes the client-side
        interface, an API that works over HTTP, dispaching all the useful data
        needed by the apps.
    \end{abstract}
    \tableofcontents
    \section{Server side}
        The server gets all the informations from NetSoul and stores it in a
        database. It then make the data easily available for any client through
        a RESTful API.

        It is implemented as a WSGI application, with python 3 and the flask
        micro framwork. Each request is dispatched according to the HTTP page
        request, then the server builds a XML tree containing the response, and
        sends it with a HTTP 200 code.

    \section{RESTful API}
        The API is a webserver that uses HTTP and POST requests to communicate
        with the different components of the project (Android app, tactile
        interface\ldots). They need to access the
        data from the netsoul server in a way that doesn't
        involve low-level sockets and complicated parsing. XML and HTTP are
        technologies that are widely used and often included in languages
        standard libraries.

        Each feature can be accessed with a dedicated page. The client sends a
        request by accessing a page through HTTP, possibly with arguments as
        POST fields, and the server sends back a XML response.

        Here is a typical XML response:
        \begin{lstlisting}
<?xml version="1.0" encoding="utf-8"?>
<ns status="ok">
    <data>data</data>
</ns>
        \end{lstlisting}

        \subsection{Status}
            Errors are reported by the status of the root node:
            \begin{lstlisting}
<ns status="error">
    <error code="n">User does not exist</error>
</ns>
            \end{lstlisting}

        \subsection{User location}
        \subsubsection{Request}
            The page is \emph{user.xml}. Parameters are:

            \begin{description}
                \item[login] the user login, in lowercase (eg. login\_x)
                \item[ip] instead of a login, we search for an IP
            \end{description}

            There is no check whether or not it is a valid username or IP.
            If there is no parameter, all the users are returned.

        \subsubsection{Response}
            If the user is connected, its location in a room is returned, as
            such:
            \begin{lstlisting}
<ns status="ok">
    <user>
        <login>login_x</login>
        <status>away/active</status>
        <location x="n" y="n">
            <room name="name" />
        </location>
        <ip version="4">a.b.c.d</ip>
        <hostname>hostname</hostname>
    </user>
</ns>
            \end{lstlisting}

            If the user is not here, the error status 1 is returned:
            \begin{lstlisting}
<ns status="error">
    <error code="1">User login_x is not
                    connected</error>
</ns>
            \end{lstlisting}
            Or, if the request was an IP, the status 2:
            \begin{lstlisting}
<ns status="error">
    <error code="2">IP a.b.c.d is not used</error>
</ns>
            \end{lstlisting}

        \subsection{Room infos}
        \subsubsection{Request}
            The page is \emph{room.xml}. Parameters are:
            \begin{description}
                \item[name] the room name (cf. section \ref{room-names} )
            \end{description}
            \fxnote{display all the machines instead of all the users?}
            If there is no parameter, all the rooms are returned.
        \subsubsection{Response}
            \begin{lstlisting}
<ns status="ok">
    <room name="name" available="yes">
        <user>
            <login>login_x</login>
            <status>away or active</status>
            <ip version="4">a.b.c.d</ip>
            <hostname>hostname</hostname>
        </user>
        <user>
            <login>login_y</login>
            <status>away or active</status>
            <ip version="4">g.h.i.j</ip>
            <hostname>hostname</hostname>
        </user>
        ...
        <reservation from="date" to="date">
            <subject></subject>
        </reservation>
        <reservation from="date" to="date">
            <subject></subject>
        </reservation>
        ...
    </room>
</ns>
            \end{lstlisting}
        \subsection{Room description}
        It is used to get a list of all rooms with the computers and their IPs.
        \subsubsection{Request}
            The page is \emph{rooms.xml}. There are no parameters.
        \subsubsection{Response}
            \begin{lstlisting}
<ns status="ok">
    <room name="name">
        <svg ...>
        </svg>
    </room>
    <room name="name">
        <svg ...>
        </svg>
    </room>
</ns>
            \end{lstlisting}
        See \ref{room-def} for informations about the svg structure.

        \subsection{News}
        The API also handles a little news feed containing the events filled in
        by the administration.
        \subsubsection{Request}
        The page is \emph{news.xml}. No argument is required.
        \subsubsection{Response}
            \begin{lstlisting}
<ns status="ok">
    <event type="conference">
        <name>...<name>
        <description>...</description>
        <date>when it was posted</date>
        <reservation from="date" to="date"
                     room="name" />
    </event>
    <event type="news">
        <name>...<name>
        <description>...</description>
        <date>when it was posted</date>
    </event>
    ...
</ns>
            \end{lstlisting}
            The events are sorted in reverse chronological order.


        \subsection{Room names}
            \label{room-names}
            The room names are:
            \begin{itemize}
                \item cisco
                \item midlab
                \item sr
                \item pasteur1
                \item pasteur2
                \item sm14
                \item sm15
                \item sm02
                \item sm03
            \end{itemize}
            \fxnote{Add VJ}

        \subsection{Dates}
            \label{dates}
            The date format used by the API is \emph{ISO 8601}.

            \emph{Rationale}: It is used by the W3C for the Web to reduce the
            complexity and error in softwares. A definition is available in the
            \href{http://tools.ietf.org/html/rfc333}{RFC 3339}. And contrary to
            timestamps. it's human readable.

            The format is \texttt{YYYY-MM-DDTHH:MM:SS}. For instance, 03/23/2012
            at 07:40 is represented as \texttt{2012-03-23T07:40:00}. The
            Wikipedia article should be a reasonable help for more informations.


        \subsection{Room definition}
            \label{room-def}
            Each room can be described with a svg file. 
            There is three kinds of elements:
            \begin{description}
                \item[walls] they are lines of class ``wall''
                \item[rows] they are lines of class ``row'', and describe the
                    computer rows.
                \item[computer] they are simple items using graphics defined in
                    the \emph{defs} part of the document. Their id are their
                    fixed IP.
            \end{description}

            It is very easy to write, as the coordinates use the number of
            computers in the room.

            Here is a complete example :
            (graphics are not as important as the elements described above. They
            are just here so that a picture of the room can easily be
            rendered)

            \begin{lstlisting}
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1"
         xmlns:xlink="http://www.w3.org/1999/xlink"
        width="800px" height="600px"
        preserveAspectRatio="none"
        viewBox="0 0 23 5">
    <!-- In order to display the room, we have to use
    a viewBox describing the user coodinates. The
    rendering then fills the space available (here
    800x600px) -->

    <defs>
    <polygon id="pc1" class="pc"
            points="0,0 -0.5,-0.2 0.5,-0.2" />
    <polygon id="pc2" class="pc"
            points="0,0 -0.5,0.2 0.5,0.2" />
    </defs>

    <line class="wall" x1="0"  y1="0"  x2="17"
          y2="0"/>
    <line class="wall" x1="0"  y1="0"  x2="0"
          y2="5"/>
          ...

    <line class="row" x1="1" y1="1" x2="16" y2="1" />
    <line class="row" x1="1" y1="2" x2="16" y2="2" />


    <use xlink:href="#pc1" x="1" y="1"
         id="10.248.1.16" class="avail"/>
    <use xlink:href="#pc2" x="2" y="1"
         id="10.248.1.15"/>
    <use xlink:href="#pc1" x="3" y="1"
         id="10.248.1.14"/>
    ...
    <use xlink:href="#pc2" x="20" y="4"
         id="10.248.4.3"/>
    <use xlink:href="#pc1" x="21" y="4"
         id="10.248.4.2" class="avail"/>
    <use xlink:href="#pc2" x="22" y="4"
         id="10.248.4.1"/>
</svg>
\end{lstlisting}


\end{document}

% vim: spell spelllang=en tw=80:
